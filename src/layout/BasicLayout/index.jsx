import React, { Component, Children } from 'react'
import SideBar from '../SideBar'
import { Container, Search ,Grid , Button, Table, Segment, Item , Image, Icon, Tab, Form, Dropdown,Radio, Input, Checkbox,Modal} from 'semantic-ui-react';
import MainContent from '../../views/MainContet'




function exampleReducer(state, action) {
    switch (action.type) {
      case 'close':
        return { open: false }
      case 'open':
        return { open: true }
      default:
        throw new Error('Unsupported action...')
    }
  }



const BasicLayout=(props) => {
    const {children} = props;
        const teloption= 
            [
                { key: '010', text: '010', value: '010' },
                { key: '011', text: '011', value: '011' },
                { key: '018', text: '018', value: '018' },
              ]
        
        const panes = [
            { menuItem: '신규주소', render: () => 
            <Tab.Pane>
                <Form>
                    <Form.Field>
                        <label className="form_column">이름 <span>*</span> </label>
                        <input required type="text" placeholder="이름을 입력하세요"/>
                        <label className="mgl15 mgr10">연락처</label>
                        <Dropdown className="mgr15" options={teloption} selection defaultValue={teloption[0].value}></Dropdown>
                        <input type="text" placeholder="전화번호를 입력"/>
                        
                    </Form.Field>
                    <Form.Field>
                    <label className="form_column inline-block">주소입력 <span>*</span> </label>
                    <Radio
                        className="mgr15"
                        label='신규주소'
                        name='radioGroup'
                        value='신규주소'
                    />
                    <Radio
                        label='기본주소'
                        name='radioGroup'
                        value='기본주소'
                    />
                    <div className="post mgt10 mgb10">
                        <input className="postnum inline-block mgr15" type="text" placeholder="우편번호"/> 
                        <Button className="postnum-btn inline-block ">검색하기</Button>
                    </div>
                    <input type="text" placeholder="주소를 입력하세요"/>
                    <p className="warning mgt10">
                        <Image className="inline-block mgr5" src="resources/icon/warning.png"></Image>
                        도로명 주소를 입력하세요
                    </p>
                    </Form.Field>
                    <Form.Field>
                        <div className="btn_group">
                            <Button secondary>초기화</Button>
                            <Button secondary>등록 취소</Button>
                            <Button primary>등록하기</Button>
                        </div>
                    </Form.Field>

                </Form>
            </Tab.Pane> },
            { menuItem: '기본주소', render: () => <Tab.Pane>Tab 2 Content</Tab.Pane> },
          ]


          const [state, dispatch] = React.useReducer(exampleReducer, {
            open: false,
          })
          const { open } = state


        return (
            <>
            {children}
            <Container fluid>
                <Grid columns={2}>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <SideBar></SideBar>
                        </Grid.Column>
                        <Grid.Column width={13} >

                                <div className="header_wrap">
                                    <Search  placeholder="키워드를 입력하세요" className="inline-block"/>
                                    <Button secondary>로그아웃</Button>
                                </div>
                                <div className="main_content_wrap">
                                    <div className="content_top">
                                    <Segment className="inline-block">
                                        <div className="content_header">
                                            주문내역
                                        </div>
                                        <Table basic="very">
                                            <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell colSpan='4'>
                                                    <div className="btn_group inline-block">
                                                    <Button content="3월"/>
                                                    <Button content="4월"/>
                                                    <Button content="5월"/>
                                                    <Button content="6월"/>
                                                    <Button content="7월"/>
                                                    <Button primary content="8월"/>
                                                    </div>
                                                    <div className="calender inline-block">
                                                        <Input className="inline-block" type="date" placeholder="2020.09.13"/>
                                                        <span className="mgl10 mgr10">-</span>
                                                        <Input className="inline-block" type="date" placeholder="2020.09.13"/>
                                                        <Button primary content="검색" className="search_btn"/>
                                                    </div>
                                                </Table.HeaderCell>
                                            </Table.Row>
                                            </Table.Header>

                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell collapsing>
                                                        <Checkbox checked/> 
                                                    </Table.Cell>
                                                    <Table.Cell collapsing >
                                                        <Image size="tiny" src="resources/images/product01.png"/>
                                                    </Table.Cell>
                                                    <Table.Cell >
                                                        <Item>
                                                            <Item.Header>보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve+ Bluetooth speaker</Item.Header>
                                                            <Item.Meta>BOSE (보스) / 미국</Item.Meta>
                                                            <Item.Description>
                                                            수량 1개
                                                            <span className="mgl10">150,000</span>원
                                                            </Item.Description>
                                                        </Item>
                                                    </Table.Cell>
                                                    <Table.Cell collapsing>
                                                        <Button secondary>주문확인</Button>
                                                    </Table.Cell>
                                                </Table.Row>

                                                <Table.Row>
                                                    <Table.Cell collapsing>
                                                        <Checkbox/> 
                                                    </Table.Cell>
                                                    <Table.Cell collapsing>
                                                        <Image size="tiny" src="resources/images/product03.png"/>
                                                    </Table.Cell>
                                                    <Table.Cell >
                                                        <Item>
                                                            <Item.Header>[셀렙샵] 셀렙샵 에디션 20FW 마이 발렌타인 드레스</Item.Header>
                                                            <Item.Meta>셀럽샵</Item.Meta>
                                                            <Item.Description>
                                                            수량 1개
                                                            <span className="mgl10">150,000</span>원
                                                            </Item.Description>
                                                        </Item>
                                                    </Table.Cell>
                                                    <Table.Cell collapsing>
                                                        <Button secondary>주문확인</Button>
                                                    </Table.Cell>
                                                </Table.Row>

                                                
                                            </Table.Body>
                                        </Table>
                                    </Segment>
                                <Segment className="inline-block">
                                        <div className="content_header">
                                            추천상품 <span>회원님의 구매내역을 분석한 추천 상품입니다.</span>
                                        </div>
                                        <div className="item_wrap">
                                            <Item  className="product_item mgr15">
                                                <Item.Image as="div" src="resources/images/product01.png" ></Item.Image>
                                                <Item.Header>보스 사운드링크 리볼브+ 블루투스 스피커SoundLink Revolve Speaker</Item.Header>
                                                <Item.Meta>
                                                <Image src="resources/icon/Member.png" className="inline-block"></Image>
                                                <span>구매한수 : 500건</span> 
                                                </Item.Meta>
                                                <Item.Description>
                                                    <Icon color="red" className="heart"/>
                                                    <span>WhisList : 1,000</span> 
                                                </Item.Description>
                                                <Item.Extra>
                                                    <span>250,000원</span>
                                                    <Button primary onClick={() => dispatch({ type: 'open'})} >
                                                        <Image src="resources/icon/Cart.png"/>
                                                    </Button>
                                                </Item.Extra>
                                            </Item>

                                            <Item  className="product_item">
                                                <Item.Image as="div" src="resources/images/product02.png" ></Item.Image>
                                                <Item.Header>칼렌듈라 허벌 엑스트렉트 토너 500ml</Item.Header>
                                                <Item.Meta>
                                                <Image src="resources/icon/Member.png" className="inline-block"></Image>
                                                <span>구매한수 : 500건</span> 
                                                </Item.Meta>
                                                <Item.Description>
                                                    <Icon className="heart"/>
                                                    <span>WhisList : 1,000</span> 
                                                </Item.Description>
                                                <Item.Extra>
                                                    <span>250,000원</span>
                                                    <Button>
                                                        <Image src="resources/icon/Cart.png"/>
                                                    </Button>
                                                </Item.Extra>
                                            </Item>
                                        </div>
                                
                                </Segment>
                                    </div>
                                    <div className="content_header">
                                        주소록 관리  <span className="block mgt10">00개의 등록된 주소가 있습니다.</span>
                                    </div>
                                <div className="content_bottom">
                                <Segment className="address">
                                    <Image className="inline-block" src="resources/icon/location.png"/>
                                    <p>신규주소 추가</p>
                                    <p>새로운 주소를 등록하세요</p>
                                </Segment>
                                <Segment>
                                    <Tab panes={panes} />
                                </Segment>
                                <Segment>
                                    <Item className="user_address">
                                        <Item.Header>
                                            <Item.Image src="resources/images/Profile Image.png" className="inline-block"></Item.Image>
                                                <div className="inline-block">
                                                <p className="company">넥스트리 회사</p>
                                                <p className="name">김유나</p>
                                                </div>
                                                <Icon className="heart"/>
                                        </Item.Header>
                                        <Item.Meta>
                                        <Icon className="check"/>
                                        <span>기본주소</span> 
                                        </Item.Meta>
                                        <Item.Description>
                                            <p>
                                            [08503]<br/>
                                            서울 금천구 가산디지털1로 171 <br/>
                                            가산 SK V1센터 101호<br/>
                                            대한민국
                                            </p>
                                        </Item.Description>
                                        <Item.Extra>
                                            Phone : +82-2-123-4567
                                        </Item.Extra>

                                        <Button.Group>
                                            <Button className="user_btn mgr15">
                                                <Image className="inline-block" src="resources/icon/수정.png"></Image>
                                                <span>수정</span>
                                            </Button>
                                            <Button className="user_btn mgr15">
                                                <Image className="inline-block" src="resources/icon/수정.png"></Image>
                                                <span>삭제</span>
                                            </Button>
                                            <Button primary>
                                                <Icon className="check"/>
                                                <span>기본배송지</span>
                                            </Button>
                                        </Button.Group>
                                        </Item>
                                </Segment>
                                </div>
                               
                                </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>


            </Container>


            <Modal
                closeIcon
                open={open}
                onClose={() => dispatch({ type: 'close' })}
                size="tiny"
            >
                <Modal.Content>
                    <Item>
                        <Item.Header>
                        장바구니 추가 완료
                        </Item.Header>
                        <Item.Description>
                            선택하신 상품이 장바구니에 추가 되었습니다.
                        </Item.Description>
                    </Item>
                </Modal.Content>
                <Modal.Actions>
                <Button size="large" centered primary onClick={() => dispatch({ type: 'close' }) }>
                    확인
                </Button>
                </Modal.Actions>
            </Modal>
                
            </>
        )
    }


export default BasicLayout;
